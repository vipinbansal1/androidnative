#include <jni.h>
#include <string>
jint  count = 0;
extern "C" JNIEXPORT jstring JNICALL
Java_com_example_apple_ndkintg_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Please click on the RED button";
    return env->NewStringUTF(hello.c_str());
}
extern "C" JNIEXPORT jint JNICALL
Java_com_example_apple_ndkintg_MainActivity_tapCountInt(
        JNIEnv* env,
        jobject obj/* this */) {
    count++;
    return count;
}